FROM ubuntu:14.04

RUN useradd -ms /bin/bash cirunner
RUN mkdir /builds && chown cirunner:cirunner /builds
USER cirunner

CMD ["bash"]
